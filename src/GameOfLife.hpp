#ifndef GameOfLife_HPP
#define GameOfLife_HPP

#include <iostream>
#include <exception>

#include <vector>
#include <string>
#include <set>

//	Minimal allowed map size
#define MIN_WIDTH   3
#define MIN_HEIGHT  3

/*
	if commented - output of the game map will be one by one
	else - will be printed at the same place
*/
#define	SAME_PLACE_PRINT


/*
	GameOfLife class that implement Conway's Game of Life algorithm
	
	The object's constructor can throw exceptions 
	because there is no member created in the heap,
	this signals the failure of creating an instance
*/
class GameOfLife {
	public:
		/*
			Default constructor take string with first generation position
			Can throw validation exception
		*/
		GameOfLife(const std::string &fpop);

		//  Copy constructor and copying operator are forbidden
		GameOfLife(const GameOfLife &src) = delete;
		GameOfLife operator = (const GameOfLife &src) = delete;

		//  data - method returns the game map view in the current state
		const std::string	data() const;

		/* 
			is_changed - method returns the state of the game map.
			True - if game map will be change, false - if not. 
		*/
		bool	is_changed() const;

		//  tick - method changed game map for one period
		void	tick();

		//  print - outputs the game map to console
		void	print() const;

		//  ValidationExeption - class that implement map validation exception
		class ValidationExeption : public std::exception {
			public:
				ValidationExeption(const char *src);
				virtual ~ValidationExeption(void) throw();

				const char *what(void) const throw();

			private:
				const char *msg;
		};

	private:

		void       		one_step(std::set<std::pair<int, int> >::iterator iter, int direction, int n);
		void       		validate() const;
		void       		initialize(const std::string &fpop);
		int        		check_neighbors(const int &x, const int &y) const;
		inline bool		is_alive(const int &x, const int &y) const;
		inline void		add_to_check(const int &x, const int &y);

		//	width and height of map
		unsigned long	_width;
		unsigned long	_height;
	
		//  _curr_state - current view of the map
		std::vector<std::string>	_curr_state;
		
		// _next_state - used as a buffer for changes
		std::vector<std::string>	_next_state;
	
		//  set of points for verification in the next period
		std::set<std::pair<int, int> >  _cells_to_check;
};

/*
	Operator comparisons returns is the state of two games map the same or not
	Can be used for unit testing
*/
bool  operator == (const GameOfLife &v1, const GameOfLife &v2);

#endif
