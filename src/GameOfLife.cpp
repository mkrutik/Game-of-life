#include "GameOfLife.hpp"

#include <algorithm>
#include <thread>
#include <mutex>
#include <cstdio>
#include <sstream>

/*
	Default constructor take first generation map as string
	parse it to lines and validate
	Can throw ValidationExeption exception
*/
GameOfLife::GameOfLife(const std::string &fpop) : _width(0), _height(0)
{
	if (fpop.size() == 0)
		throw ValidationExeption("Empty file not allowed !");

	//	initialize first generation map and validate it
	initialize(fpop);
	validate();
}

//  data - method returns the game map view in the current state
const std::string	GameOfLife::data() const 
{
	std::string res;

	for (auto line : _curr_state)
	{
		res.append(line);
		res.push_back('\n');
	}

	return res;
}

//  is_changed - method returns the state of the game map
bool	GameOfLife::is_changed() const
{
	return (_cells_to_check.size() != 0);
}

void	GameOfLife::one_step(std::set<std::pair<int, int> >::iterator iter, int direction, int n)
{
	while (n-- > 0)
	{
		//	get point from checklist
		std::pair<int, int> point = *iter;

		// iterate according to direction
		iter = std::next(iter, direction);

		// check is point in visibility zone
		if (point.first < 0 || static_cast<size_t>(point.first) >= _width ||
			point.second < 0 || static_cast<size_t>(point.second) >= _height)
				continue;

		//	check how many living neighbors
		int n = check_neighbors(point.first, point.second);

		//	check state of current point
		bool stat = is_alive(point.first, point.second);

		//	change the state of the point according to the rules
		if (stat && n != 2 && n != 3)
		{
			_next_state.at(point.second).at(point.first) = '0';
			add_to_check(point.first, point.second);
		}
		else if (!stat && n == 3)
		{
			_next_state.at(point.second).at(point.first) = '1';
			add_to_check(point.first, point.second);
		}
	}
}

//	// apply one period changes
void	GameOfLife::tick()
{
	//	create a temporary variable and copy there a set of points to check
	std::set<std::pair<int, int> >   curr_check = _cells_to_check;

	//	clear main points checklist 
	_cells_to_check.clear();

	//	count size of checklist
	int size = curr_check.size();

	//	create two threads for calculation
	std::thread first(&GameOfLife::one_step, this, curr_check.begin(), 1, (size / 2));
	std::thread second(&GameOfLife::one_step, this, --curr_check.end(), -1, (size - (size / 2)));

	//	wait for threads to end
	first.join();
	second.join();

	// clear current map state and re-assignment of the current state
    _curr_state.clear();
    _curr_state = _next_state;
}

// print - method that print to console colorized game map
void	GameOfLife::print() const
{
	//	used to change output style
	#ifdef SAME_PLACE_PRINT
		//  print esc sumbol for reprint map at the same place
		for (unsigned long i = 0; i < _height + 2; i++)
			printf("%c[1A", 0x1B);
	#endif

	//  print top frame
	for (unsigned long i = 0; i < _width + 2; i++)
		std::cout << "-";
	std::cout << std::endl;

	for (const auto &line : _curr_state)
	{
		// print left side frame
		std::cout << "|";

		// print a view of element
		for (const auto &cell : line)
		{
			// print alive cell in green color
			if (cell == '1')
				std::cout << "\e[92m" << "*" << "\e[39m";
			else
				std::cout << " ";
		}

		// print right side frame
		std::cout << "|" << std::endl;
	}

	//  print bottom frame
	for (unsigned long i = 0; i < _width + 2; i++)
		std::cout << "-";
	std::cout << std::endl;
}

//  validate - method validate first generation map
void	GameOfLife::validate() const
{
	// function - line checker
	auto line_checker = [](char c) {
		if (c != '1' && c != '0')
			throw ValidationExeption("One or more characters in the map are not supported, only 1 and 0 are allowed - see Readme");
		};

	// check that map has mimal size
	if (_width < MIN_WIDTH || _height < MIN_HEIGHT)
		throw ValidationExeption("The size of map is less than allowed");
	
	//  checking all rows
	for (const auto &line : _curr_state)
	{
		//  check that all rows are the same length
		if (line.size() != _width)
			throw ValidationExeption("One or more rows have different lengths. The map must be rectangular");      
		
		//  check for valid characters
		std::for_each(line.begin(), line.end(), line_checker);
	}
}

/*
	initialize - method take first generation map
	and fills the internal representation of the map
	and checklist for next tick
*/
void	GameOfLife::initialize(const std::string &fpop)
{
	// use to select separated rows
	std::istringstream input(fpop);
	std::string line;

	// get a separate line and process it
	while (std::getline(input, line))
	{
		// fisrt initialization of _width variable
		if (_height == 0)
			_width = line.size();

		for (unsigned x = 0; x < line.size(); x++)
		{
			// if point alive add to first checklist this point add his neighbors
			if (line.at(x) == '1')
				add_to_check(x, _height);
		}

		_curr_state.push_back(line);
		++_height;
	}
 
	_next_state = _curr_state;
}

/*
	check_neighbors - method take point coordinates
	and check how many alive neighbors it has
*/
int	GameOfLife::check_neighbors(const int &x, const int &y) const
{
	// coordinates of neighbors points
	std::pair<int, int>	neighbors[8] = {
		std::make_pair(x - 1, y - 1),
		std::make_pair(x, y - 1),
		std::make_pair(x + 1, y - 1),
		std::make_pair(x - 1, y),
		std::make_pair(x + 1, y),
		std::make_pair(x - 1, y + 1),
		std::make_pair(x    , y + 1),
		std::make_pair(x + 1, y + 1),
		};

	int n = 0;

	for (const auto &point : neighbors)
	{
		// check is neighbor point in visibility zone
		if (point.first < 0 || static_cast<size_t>(point.first) >= _width || 
			point.second < 0 || static_cast<size_t>(point.second) >= _height)
			continue;

		// check is neighbor point alive
		if (is_alive(point.first, point.second))
			n++;
	}
	return n;
}

// is_alive - method take point coordinates and check is it alive
bool       GameOfLife::is_alive(const int &x, const int &y) const
{
	return (_curr_state.at(y).at(x) == '1');
}

// add_to_check - method that add point and it neighbors to checklist on next tick
void	GameOfLife::add_to_check(const int &x, const int &y)
{
	/*
		insertion to set isn`t thread safe
		we need to use mutex
	*/
	static std::mutex mut;
	std::lock_guard<std::mutex> lock(mut);


	_cells_to_check.insert(std::make_pair(x, y));
	_cells_to_check.insert(std::make_pair(x - 1, y - 1));
	_cells_to_check.insert(std::make_pair(x, y - 1));
	_cells_to_check.insert(std::make_pair(x + 1, y - 1));
	_cells_to_check.insert(std::make_pair(x - 1, y));
	_cells_to_check.insert(std::make_pair(x + 1, y));
	_cells_to_check.insert(std::make_pair(x - 1 , y + 1));
	_cells_to_check.insert(std::make_pair(x, y + 1));
	_cells_to_check.insert(std::make_pair(x + 1, y + 1));
}

// compare the state of two game maps
bool	operator == (const GameOfLife &v1, const GameOfLife &v2)
{
	return (v1.data().compare(v2.data()) == 0);
}

//  default ValidationExeption constructor take error message
GameOfLife::ValidationExeption::ValidationExeption(const char *src) : msg(src) {}

GameOfLife::ValidationExeption::~ValidationExeption(void) throw() {}

// what - overloaded method that return msg error
const char*		GameOfLife::ValidationExeption::what(void) const throw()
{
	return msg;
}
