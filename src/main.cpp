#include <iostream>
#include "fstream"
#include <sstream>
#include <thread>
#include <chrono>
#include <memory>

#include "GameOfLife.hpp"

/*
	read_file - function that reads from a file (filename)
	and returns the readed information as a string
	Can throw exeption.
*/
std::string	read_file(const char *filename)
{
	std::stringstream	buffer;
	std::ifstream		file(filename);

	file.exceptions(std::ifstream::failbit);

	buffer << file.rdbuf();

	file.close();
	return buffer.str();
}

void	game_loop(const std::unique_ptr<GameOfLife> &p)
{
	//	create time points for implement FPS
	std::chrono::system_clock::time_point a = std::chrono::system_clock::now();
	std::chrono::system_clock::time_point b = std::chrono::system_clock::now();

	//	print first generation game map
	p->print();

	// game ends when nothing changes on the map
	while (p->is_changed())
	{
		// apply one period changes
		p->tick();

		a = std::chrono::system_clock::now();
	
		//	count the running time of the algorithm at the current iteration
		std::chrono::duration<double, std::milli>	work_time = a - b;

		if (work_time.count() < 50)
		{
			//	measure the time for thread sleep
			std::chrono::duration<double, std::milli>	delta(50 - work_time.count());
			auto delta_ms_duration = std::chrono::duration_cast<std::chrono::milliseconds>(delta);
			std::this_thread::sleep_for(std::chrono::milliseconds(delta_ms_duration.count() ));
		}

		b = std::chrono::system_clock::now();

		//	print new game map state
		p->print();
	}

	std::cout << "Game over, population doesn`t change anymore" << std::endl;
}

int	main(int argc, char **argv)
{
	// allowed only one argument - input file name
	if (argc != 2)
	{
		std::cout << "Usage: ./GameOfLife [input file name]" << std::endl;
		return (-1);
	}

	std::unique_ptr<GameOfLife> p;

	try
	{
		// try to open and read from file
		std::string data = read_file(argv[1]);

		// can throw validation exception
		p = std::unique_ptr<GameOfLife>(new GameOfLife(data));
	}
	catch(std::exception &e)
	{
		std::cout << e.what() << std::endl;
		return (-1);
	}

	game_loop(p);

	return (0);
}