# Conway's Game of Life

The Game of Life is a cellular automaton devised by the British mathematician John Horton Conway in 1970.
It is the best-known example of a cellular automaton.

The game is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input.
One interacts with the Game of Life by creating an initial configuration and observing how it evolves, or, for advanced players, by creating patterns with particular properties.

Conway's Game of Life is described [here](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).

## Implementation detail

The operation of the game starts with an initial configuration on a two dimensional grid. This **limited** square grid consists of cells with two possible states, alive or dead. Each cell has eight neighbors, namely the eight cells that touch it. The game operates in iterations, called ticks. Each tick applies the four rules of the game to every cell on the board simultaneously.

### Example of initialization file

```
000000000000000
000000000000000
000000000000000
000111111011000
000111111011000
000000000011000
000110000011000
000110000011000
000110000011000
000110000000000
000110111111000
000110111111000
000000000000000
000000000000000
000000000000000
```

The file is a **rectangular** map consisting entirely of *zeros* (dead cells) and *ones* (live).
Minimal map size 3 x 3. Presence of other symbols will be interpreted as an error.

Another examples of initialization files can be found in the __*initconfig*__ directory.

### The main rules of the game
- Any live cell with fewer than two live neighbors dies, as if caused by underpopulation.
- Any live cell with more than three live neighbors dies, as if by overcrowding.
- Any live cell with two or three live neighbors lives on to the next generation.
- Any dead cell with exactly three live neighbors becomes a live cell.

### Build project

```
    cmake . && make
```

### Run game

```
    ./GameOfLife [congiguration file]
```

### Output configuration

The game is implemented on only C++ and STL that's why output of game goes to console.
By default game output reprint on the same position and you can see the only process of change.
If you want to see the order of the change - comment out this one line in file src/GameOfLife.hpp and rebuild project.

```
    #define SAME_PLACE_PRINT
```

Should look like this after commenting:
```
    // #define SAME_PLACE_PRINT
```
